import express, { Request, Response } from "express";
import {  UserController} from "../controller/UsersController";
import {LogInfo} from "../utils/logger"

// Router from express

let usersRouter = express.Router();

//http://localhost:8000/api/users?id=
usersRouter.route('/')
// GET  -> http://localhost:8000/api/users?id=
    .get(async (req: Request, res: Response) => {
        // Obtain a query param
        let id: any = req?.query?.id;
        LogInfo(`Query param: ${id}`);
        // Controller Instance
        const controller: UserController = new UserController();
        // Obtain response
        const response: any = await controller.getUsers(id)
        // Send to the client
        return res.send(response)
    })
    .delete(async (req:Request,res:Response) => {
         // Obtain a query param
         let id: any = req?.query?.id;
         LogInfo(`Query param: ${id}`);
         // Controller Instance
         const controller: UserController = new UserController();
         // Obtain response
         const response: any = await controller.deleteUser(id)
         // Send to the client
         return res.send(response)
    })
    .post(async(req:Request, res:Response) => {

         let name: any = req?.query?.name;
         let age: any = req?.query?.age;
         let email: any = req?.query?.email;
         
         // Controller Instance
         const controller: UserController = new UserController();
         

         let user = {
            name: name || "default name",
            email: email || "default email",
            age: age || 18
         }
         // Obtain response
         const response: any = await controller.createUser(user)
         // Send to the client
         return res.send(response)
    })
    .put(async (req:Request, res: Response) => {
     let name: any = req?.query?.name;
     let age: any = req?.query?.age;
     let email: any = req?.query?.email;
     let id: any = req?.query?.id;
     
     // Controller Instance
     const controller: UserController = new UserController();
     

     let user = {
        name: name,
        email: email,
        age: age
     }
     // Obtain response
     const response: any = await controller.updateUser(user, id)
     // Send to the client
     return res.send(response)
    })

    // Export HelloRouter
    export default usersRouter