/**
 * Root Router
 * Redirections to Routers
 */

import express, { Request, Response } from 'express'
import helloRouter from './HelloRoutes'
import { LogInfo } from '../utils/logger'
import goodbyeRouter from './GoodbyeRoutes';
import usersRouter from './UserRouter';

// Server instance
const server = express();

// Router instance
let rootRouter = express.Router();

// Activate for requests to http://localhost:8000/api/
// GET: http://localhost:8000/api/
rootRouter.get('/', (req: Request, res: Response) => {
    LogInfo('GET: http://localhost:8000/api/')
    // Send Hello World
    res.send('welcome to the API restful: Express + TS + swagger');
});

// Redirections to Routers & controllers
server.use('/', rootRouter); // http://localhost:8000/api/
server.use('/hello', helloRouter) // http://localhost:8000/api/hello --> HelloRouter
server.use('/goodbye', goodbyeRouter) // http://localhost:8000/api/users --> UserRouter

// Add more routes to the app

server.use('/users', usersRouter)

export default server
