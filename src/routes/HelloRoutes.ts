import express, { Request, Response } from "express";
import { HelloController} from "../controller/HelloController";
import {LogInfo} from "../utils/logger"

// Router from express

let helloRouter = express.Router();

//http://localhost:8000/api/hello?name=queryparam
helloRouter.route('/')
// GET  -> http://localhost:8000/api/hello?name=queryparam
    .get(async (req: Request, res: Response) => {
        // Obtain a query param
        let name: any = req?.query?.name;
        LogInfo(`Query param: ${name}`);
        // Controller Instance
        const controller: HelloController = new HelloController();
        // Obtain response
        const response = await controller.getMessage(name)
        // Send to the client
        return res.send(response)
    })

    // Export HelloRouter
    export default helloRouter