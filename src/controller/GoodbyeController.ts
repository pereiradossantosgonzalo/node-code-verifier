import { GoodbyeResponse } from "./types";
import { IGoodbyeController } from "./interfaces";
import { LogSuccess } from "../utils/logger";

export class GoodbyeController implements IGoodbyeController {
    public async getMessage(name?: string | undefined): Promise<GoodbyeResponse> {
        LogSuccess('[api/goodbye], GET request')
        
        return {
            message: `Goodbye ${name || "World"}`,
            date: new Date()
        }
    }
}