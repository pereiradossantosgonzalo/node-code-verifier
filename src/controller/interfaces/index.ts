import { BasicResponse } from "../types";
import { GoodbyeResponse } from "../types";

export interface IHelloController {
    getMessage(name?:string): Promise<BasicResponse>
}

export interface IGoodbyeController {
    getMessage(name?:string): Promise<GoodbyeResponse>
}

export interface IUserController {

    // Read all users from database || get user by ID
    getUsers(id?: String): Promise<any>

    // Delete user by ID
    deleteUser(id?: String): Promise<any>
    
    // Create new User
    createUser(user:any): Promise<any>

    // Update User
    updateUser(id:string, user:any): Promise<any>
}