export type BasicResponse = {
    message: string
};

export type GoodbyeResponse = {
    message: string,
    date: Date
}

export type ErrorResponse = {
    error: string,
    message: string
}