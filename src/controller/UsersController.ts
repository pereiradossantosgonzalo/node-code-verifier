import { Get, Route, Tags, Query } from "tsoa";
import { IUserController } from "./interfaces";
import { LogSuccess, LogError, LogWarning } from "../utils/logger";

// ORM - Users Collection

import { getAllUsers, getUserByID, deleteUserByID, createUser, updateUserByID } from "../domain/orm/User.orm";


@Route("/api/users")
@Tags("UserController")

export class UserController implements IUserController {
   
    
    /**
     * Endpoint to retrieve the Users in the collection "Users" of DB
     */
    public async getUsers(@Query()id?: string): Promise<any> {

        let response: any = ''
        
        if(id){

            LogSuccess(`[/api/users] Delete User by ID: ${id}`);

            return response = await getUserByID(id)

        } else {

            LogSuccess('[/api/users] Get All Users Request');

            response = await getAllUsers();       

        }

       

        return response
    }

    public async deleteUser(@Query()id?: string): Promise<any> {

        let response: any = ''
        
        if(id){

            LogSuccess(`[/api/users] Get User by ID: ${id}`);

            await deleteUserByID(id).then((r)=> {
                response = {
                    message: `User with id ${id} deleted succesfully`
                }
           })

        } else {

            LogWarning('[/api/users] Delete User Request WITHOUT ID');

            response = {
                message: "please provide an ID to delete"
            }       

        }

       

        return response
    }

    public async createUser(user: any): Promise<any> {

        let response: any = ''
        
        await createUser(user).then((r) => {
            LogSuccess(`[/api/users] Created new User: ${user}`);
            response = {
                message: `User ${user.name} created successfully`
            }
        })
        return response
    }

    public async updateUser( user:any, id:string ): Promise<any> {
        let response: any = ''
        
        if(id){

            LogSuccess(`[/api/users] Update User by ID: ${id}`);

            await updateUserByID(user, id).then((r)=> {
                response = {
                    message: `User with id ${id} updated succesfully`
                }
           })

        } else {

            LogWarning('[/api/users] Update User Request WITHOUT ID');

            response = {
                message: "please provide an ID to delete"
            }       

        }

        return response
    }

}