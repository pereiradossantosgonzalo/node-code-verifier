import { kataEntity } from "../entities/Kata.entity";
import { LogSuccess, LogError } from "../../utils/logger";

export const GetAllKata = async (): Promise<any[] | undefined> => {
    try {
        let kataModel = kataEntity()
        return await kataModel.find({isDelete: false})
    } catch (error) {
        LogError(`[ORM ERROR]: Getting al Kata: ${error}`)
    }
}