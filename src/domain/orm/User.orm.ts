import { userEntity } from "../entities/User.entity";

import { LogError, LogSuccess } from "../../utils/logger";

// CRUD PETITIONS

/**
 * Method to obtain all users from collection "Users" in Mongo Server
 */
export const getAllUsers = async (): Promise<any[] | undefined> => {

    try {
        let userModel = userEntity();

        // Search all users
        return await userModel.find({isDelete:false})

    } catch (error) {
        
        LogError(`[ORM ERROR]: Getting All Users: ${error}`)

    }

}

   
// Get User By ID
export const getUserByID = async (id: string) : Promise <any | undefined> => {

    try {
        let userModel = userEntity();

        // Search User by id
        return await userModel.findById(id)

    } catch (error) {
        
        LogError(`[ORM ERROR]: Getting User by ID: ${error}`)

    }
}

// Delete user By ID
export const deleteUserByID = async (id:string): Promise <any | undefined> => {

    try {
        let userModel = userEntity()
        
        return await userModel.deleteOne({_id: id})

    } catch (error) {
        LogError(`[ORM ERROR]: Deleting User by ID: ${error}`)

    }


}
// Create User
export const createUser = async(user:any): Promise<any | undefined> => {
    try {

        let userModel = userEntity();

        // Create / insert new user

        return await userModel.create(user);
        
    } catch (error) {
        LogError(`[ORM ERROR]: Creating User: ${error}`)
    }
}


// Update User by ID
export const updateUserByID = async (user:any, id:string): Promise<any | undefined> => {
    try {
        let userModel = userEntity();

        // Update User
        return await userModel.findByIdAndUpdate(id, user);
    } catch (error) {
        LogError(`[ORM ERROR]: Updating User ${id}: ${error}`)
    }
}

// TODO: 
// Get User By email



