import mongoose from "mongoose";

export const kataEntity = () => {

    let kataSchema = new mongoose.Schema({
        Name: String,
        Description: String,
        Level: Number,
        Date: Date,
        Valoration: Number,
        Chances: Number
    })

    return mongoose.model('Kata', kataSchema)

}